package com.release.logger;

import android.util.Log;

public class Logger {
    /**
     * @param TAG
     * @param message
     */
    public static void d(String TAG, String message) {
        Log.d(TAG, message);
    }
}
